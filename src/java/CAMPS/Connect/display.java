/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Connect;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class display extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DBConnect db = new DBConnect();
        try {
            db.getConnection();
            // System.out.println("sgsfd");
            db.read("select * from 161CS293.studentdetails");
            //out.print(<input type="text" name="studentname"><br>);
 //student name:<input type="text" name="studentid"><br>
            out.print("<table border='1'><tr><th>Studentid</th><th>studentname</th><th>studentbatch</th><th>Phonenum</th><th>status</th><th>inserteddate</th><th>delete</th><th>Status</th></tr>");
            while (db.rs.next()) {
                out.print("<tr><td>" + db.rs.getString("studentid") + "</td><td>" + db.rs.getString("studentname") + "</td><td>" + db.rs.getString("studentbatch") + "</td><td>" + db.rs.getString("phonenum") + "</td><td>" + db.rs.getString("status") + "</td><td>" + db.rs.getString("inserteddate") + "</td><td><input type='button' value='Delete' onclick=deletion(" + db.rs.getString("studentid") + ")></td>");
                //out.println("<div id=' result '></div>");
                if ("continue".equals(db.rs.getString("status"))) {
                    out.print("<td><input type='button' value='Discontinue' onclick=status(" + db.rs.getString("studentid") + ")></td></tr>");
                } else {
                    out.print("<td><input type='button' value='Continue' onclick=status(" + db.rs.getString("studentid") + ")></td></tr>");
                }
            }

            out.println("</table>");
        } catch (Exception e) {
            out.println(e);
        } finally {
            db.closeConnection();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(display.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
